﻿namespace Vote.Data
{
    public partial class VoteItemDto
    {
        public bool YN { get; set; }
        public int Count { get; set; }
        public int MaleCount { get; set; }
        public int FemaleCount { get; set; }
    }

    public partial class VoteItem
    {
        public bool YN { get; set; }
        public int Count { get; set; }
        public int MaleCount { get; set; }
        public int FemaleCount { get; set; }
    }

    public partial class Member
    {
        public string Password2 { get; set; }
    }

    public partial class MemberDto
    {
        public string Password2 { get; set; }
    }

    public partial class GroupDto
    {
        public bool YN { get; set; }
    }

    public partial class Group
    {
        public bool YN { get; set; }
    }
}