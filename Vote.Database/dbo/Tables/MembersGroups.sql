﻿CREATE TABLE [dbo].[MembersGroups] (
    [MemberId] INT NOT NULL,
    [GroupId]  INT NOT NULL,
    CONSTRAINT [PK_MembersGroups] PRIMARY KEY CLUSTERED ([MemberId] ASC, [GroupId] ASC),
    CONSTRAINT [FK_MembersGroups_Groups] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Groups] ([Id]),
    CONSTRAINT [FK_MembersGroups_Members] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Members] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_MembersGroups]
    ON [dbo].[MembersGroups]([MemberId] ASC);

