﻿CREATE TABLE [dbo].[Members] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (50) NOT NULL,
    [Gender]     NVARCHAR (5)  NOT NULL,
    [Birthday]   DATE          NOT NULL,
    [Email]      VARCHAR (50)  NOT NULL,
    [Password]   VARCHAR (100) NOT NULL,
    [Token]      VARCHAR (100) NULL,
    [IsAdmin]    BIT           NOT NULL,
    [CreateDate] DATETIME      NOT NULL,
    [ModifyDate] DATETIME      NOT NULL,
    CONSTRAINT [PK_Members] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [IX_Members] UNIQUE NONCLUSTERED ([Id] ASC)
);

