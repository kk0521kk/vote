﻿CREATE TABLE [dbo].[VoteItemsMembers] (
    [VoteItemId] INT NOT NULL,
    [MemberId]   INT NOT NULL,
    CONSTRAINT [PK_VoteItemsMembers] PRIMARY KEY CLUSTERED ([VoteItemId] ASC, [MemberId] ASC),
    CONSTRAINT [FK_VoteItemsMembers_Members] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Members] ([Id]),
    CONSTRAINT [FK_VoteItemsMembers_VoteItems] FOREIGN KEY ([VoteItemId]) REFERENCES [dbo].[VoteItems] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_VoteItemsMembers]
    ON [dbo].[VoteItemsMembers]([VoteItemId] ASC);

