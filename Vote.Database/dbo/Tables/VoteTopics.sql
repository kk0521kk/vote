﻿CREATE TABLE [dbo].[VoteTopics] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (20) NOT NULL,
    [Description]    NVARCHAR (80) NULL,
    [IsActive]       BIT           NOT NULL,
    [GroupId]        INT           NOT NULL,
    [AllowItems]     INT           NOT NULL,
    [CanAddItems]    BIT           NOT NULL,
    [ExpireDate]     DATETIME      NOT NULL,
    [CreateMemberId] INT           NOT NULL,
    [CreatedDate]    DATETIME      NOT NULL,
    CONSTRAINT [PK_VoteTopics] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_VoteTopics_Groups] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Groups] ([Id]),
    CONSTRAINT [FK_VoteTopics_Members] FOREIGN KEY ([CreateMemberId]) REFERENCES [dbo].[Members] ([Id])
);

