﻿CREATE TABLE [dbo].[VoteItems] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [VoteTopicId]    INT           NOT NULL,
    [Name]           NVARCHAR (20) NOT NULL,
    [Description]    NVARCHAR (50) NULL,
    [CreatedDate]    DATE          NOT NULL,
    [CreateMemberId] INT           NOT NULL,
    CONSTRAINT [PK_VoteItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_VoteItems_Members] FOREIGN KEY ([CreateMemberId]) REFERENCES [dbo].[Members] ([Id]),
    CONSTRAINT [FK_VoteItems_VoteTopics] FOREIGN KEY ([VoteTopicId]) REFERENCES [dbo].[VoteTopics] ([Id])
);

