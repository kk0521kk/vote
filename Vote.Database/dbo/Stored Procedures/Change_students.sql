﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Change_students
	-- Add the parameters for the stored procedure here
	@studentID int, @st_ID nvarchar(50), @LastName varchar(255), @FirstName varchar(255)
AS
DECLARE	
	@Age int, @Birthday datetime
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @Age=Age, @Birthday = Birthday
	FROM students
	Where @st_ID = st_ID;
END