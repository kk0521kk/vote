﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Vote.Data;

namespace Vote.Service
{
    public class VoteService : BaseService
    {
        private static VoteService _instance;

        public static VoteService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new VoteService();
                }
                return _instance;
            }
        }

        public List<VoteTopicDto> GetAllVoteTopics()
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.VoteTopics.Select(x => x).ToList();
                    result.ForEach(x => x.VoteItems = new List<VoteItem>());
                    var dto = _mapper.Map<List<VoteTopicDto>>(result);
                    return dto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<VoteTopicDto> GetTopicsByKey(string key)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.VoteTopics.Where(x => x.Name.Contains(key) || x.Description.Contains(key)).ToList();
                    var dto = _mapper.Map<List<VoteTopicDto>>(result);
                    return dto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //得到某一題的詳細
        public VoteTopicDto GetVoteTopicAndItems(int TopicId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.VoteTopics.Include("VoteItems").SingleOrDefault(x => x.Id == TopicId);
                    if (result == null)
                    {
                        return null;
                    }
                    var voteTopicDto = _mapper.Map<VoteTopicDto>(result);
                    voteTopicDto.VoteItems = _mapper.Map<List<VoteItemDto>>(voteTopicDto.VoteItems);

                    return voteTopicDto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //得到某個會員問過的問題
        public List<VoteTopicDto> GetMemberAskedVoteTopics(int memberId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var topics = context.VoteTopics.Where(x => x.CreateMemberId == memberId).ToList();
                    var topicsDto = _mapper.Map<List<VoteTopicDto>>(topics);
                    return topicsDto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //得到member的類別的題目,
        public List<VoteTopicDto> GetMemberVoteTopics()
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var MemberId = 1;
                    var memberGroupList = context.Database.SqlQuery<int>("Select groupId from MembersGroups where MemberId = {0} ", MemberId).ToList();
                    var result = context.VoteTopics.Where(x => memberGroupList.Contains(x.GroupId)).ToList();

                    var dto = _mapper.Map<List<VoteTopicDto>>(result);

                    return dto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //建立題目
        public Tuple<Boolean, string, VoteTopicDto> SaveVoteTopic(VoteTopicDto data, int memberId)
        {
            try
            {
                var context = new VoteEntities();
                VoteTopic voteTopic = new VoteTopic
                {
                    CreatedDate = DateTime.Now,
                    CreateMemberId = memberId,
                    Name = data.Name,
                    GroupId = data.GroupId,
                    AllowItems = data.AllowItems,
                    Description = data.Description,
                    ExpireDate = data.ExpireDate,
                    IsActive = data.IsActive,
                    CanAddItems = data.CanAddItems
                };
                context.VoteTopics.Add(voteTopic);

                List<string> itemNameList = new List<string>();

                foreach (var item in data.VoteItems)
                {
                    var voteItem = _mapper.Map<VoteItem>(item);
                    itemNameList.Add(item.Name);
                    voteItem.CreatedDate = DateTime.Now;
                    voteItem.CreateMemberId = memberId;
                    voteTopic.VoteItems.Add(voteItem);
                }
                if (itemNameList.Count == 0)
                {
                    return new Tuple<Boolean, string, VoteTopicDto>(false, "沒有選項ㄟ==", null);
                }
                //判斷有沒有出現重複選項
                for (int i = 0; i < itemNameList.Count; i++)
                {
                    for (int j = itemNameList.Count - 1; j > i; j--)
                    {
                        if (itemNameList[i] == itemNameList[j])
                        {
                            return new Tuple<Boolean, string, VoteTopicDto>(false, "有重複的選項", null);
                        }
                    }
                }

                var result = context.SaveChanges() == 1;
                var topicId = voteTopic.Id;
                var topicDto = GetVoteTopicAndItems(topicId);

                return new Tuple<Boolean, string, VoteTopicDto>(true, "成功", topicDto);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex", ex);
                throw ex;
            }
        }

        public int DeleteVoteTopic(int memberId, int topicId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var entity = context.VoteTopics.SingleOrDefault(x => x.Id == topicId && x.CreateMemberId == memberId);
                    if (entity == null)
                    {
                        Debug.WriteLine("entity == null");
                        return 0;
                    }
                    else
                    {
                        entity.IsActive = false;
                    }
                    return context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex", ex);
                throw ex;
            }
        }

        //會員投票
        public Tuple<bool, string> MemberVote(VoteTopicDto voteTopic, int memberId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var topic = context.VoteTopics.SingleOrDefault(x => x.Id == voteTopic.Id);
                    var count = 0;

                    var itemList = context.VoteItems.Where(x => x.VoteTopicId == topic.Id).Select(y => y.Id).ToList();
                    var sql = @"Delete from VoteItemsMembers where MemberId = {0} and VoteItemId = {1}";
                    foreach (var i in itemList)
                    {
                        context.Database.ExecuteSqlCommand(sql, memberId, i);
                    }

                    var sql2 = @"Insert into VoteItemsMembers values  ({0}, {1});";
                    //存進db
                    foreach (var item in voteTopic.VoteItems)
                    {
                        try
                        {
                            if (item.YN)
                            {
                                count++;
                                if (topic.AllowItems < count)
                                {
                                    foreach (var i in itemList)
                                    {
                                        context.Database.ExecuteSqlCommand(sql, memberId, i);
                                    }
                                    return new Tuple<bool, string>(false, "這題只能選" + topic.AllowItems + "個選項喔");
                                }
                                context.Database.ExecuteSqlCommand(sql2, item.Id, memberId);
                            }
                        }
                        catch (Exception ex)
                        {
                            return new Tuple<Boolean, string>(false, ex.Message);
                        }
                    }
                    if (count == 0)
                    {
                        return new Tuple<bool, string>(false, "你甚麼也沒選喔==");
                    }

                    var result = context.SaveChanges() == 1;
                    //return result == false????
                    return new Tuple<Boolean, string>(true, "成功");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //得到某題的每個選項有幾票
        public VoteTopicDto GetVoteResult(int topicId, int memberId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var itemsMembers = context.VoteItemsMembersInfoes.Where(x => x.VoteTopicId == topicId).ToList();
                    var itemsMembersDto = _mapper.Map<List<VoteItemsMembersInfo>>(itemsMembers);
                    var topic = context.VoteTopics.Include("VoteItems").SingleOrDefault(x => x.Id == topicId);
                    Debug.Print("memberId" + memberId);

                    topic.VoteItems.ToList().ForEach(x =>
                        {
                            var data = itemsMembersDto.Where(y => y != null && y.MemberId != null && y.VoteItemId == x.Id).ToList();
                            x.Count = data == null ? 0 : data.Count();
                            x.YN = itemsMembersDto.Where(z => z.MemberId == memberId && z.VoteItemId == x.Id).Count() > 0;
                            if (x.YN)
                            {
                                Debug.Print(x.Name);
                            }
                        }
                    );
                    var result = _mapper.Map<VoteTopicDto>(topic);
                    result.VoteItems = _mapper.Map<List<VoteItemDto>>(topic.VoteItems);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //得到某題的每個選項是那些人投
        public VoteTopicDto GetVoteResultDetail(int topicId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var itemsMembers = context.VoteItemsMembersInfoes.Where(x => x.VoteTopicId == topicId).ToList();
                    var itemsMembersDto = _mapper.Map<List<VoteItemsMembersInfo>>(itemsMembers);
                    var topic = context.VoteTopics.Include("VoteItems").SingleOrDefault(x => x.Id == topicId);

                    var result = _mapper.Map<VoteTopicDto>(topic);

                    result.VoteItems = _mapper.Map<List<VoteItemDto>>(topic.VoteItems);
                    result.VoteItems.ToList().ForEach(x =>
                        {
                            var data = itemsMembersDto.Where(y => y != null && y.MemberId != null && y.VoteItemId == x.Id).ToList();
                            x.Count = data == null ? 0 : data.Count();
                            x.FemaleCount = data.Where(y => y.Gender.Equals("F")).Count();
                            x.MaleCount = data.Where(y => y.Gender.Equals("M")).Count();
                            x.Members = _mapper.Map<List<MemberDto>>(data);
                        }
                    );

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //得到已經結束的問題(會員的類別)的topic
        public List<VoteTopicDto> GetDoneMemberVoteTopics()
        {
            var result = GetMemberVoteTopics().Where(x => x.ExpireDate < DateTime.Now && x.IsActive == true).ToList();
            return result;
        }

        public int GetCountMemberVotedTopics(int memberId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.VoteItemsMembersInfoes.Where(y => y.MemberId == memberId).ToList();
                    int count = result.GroupBy(x => x.VoteTopicId).Count();
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}