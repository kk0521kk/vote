﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vote.Data;

namespace Vote.Service
{
    public class GroupService : BaseService
    {
        private static GroupService _instance;

        public static GroupService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GroupService();
                }
                return _instance;
            }
        }

        public List<GroupDto> GetAllGroups()
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.Groups.Select(x => x).ToList();
                    var dto = _mapper.Map<List<GroupDto>>(result);
                    return dto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GroupDto GetGroupVoteTopics(int groupId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.Groups.Include("VoteTopics").SingleOrDefault(x => x.Id == groupId);

                    var group = _mapper.Map<GroupDto>(result);
                    group.VoteTopics = _mapper.Map<List<VoteTopicDto>>(group.VoteTopics);
                    return group;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tuple<int, List<GroupDto>> GetAllGroupsWithMemberGroups(int memberId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var MemberGroupsList = context.MembersGroupsInfoes.Where(x => x.MemberId == memberId).Select(y => y.GroupId).ToList();
                    var groups = context.Groups.Select(x => x).ToList();
                    var groupDto = _mapper.Map<List<GroupDto>>(groups);
                    int count = 0;
                    foreach (var item in groupDto)
                    {
                        if (MemberGroupsList.Contains(item.Id))
                        {
                            item.YN = true;
                            count++;
                        }
                    }
                    return new Tuple<int, List<GroupDto>>(count, groupDto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //管理者功能
        public Tuple<bool, string> AddGroups(GroupDto group)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var entity = context.Groups.SingleOrDefault(x => x.Name == group.Name);
                    if (entity != null)
                    {
                        return new Tuple<bool, string>(false, "類別重複了喔");
                    }
                    {
                        entity = _mapper.Map<Group>(group);
                        context.Groups.Add(entity);
                    }
                    var result = context.SaveChanges() == 1;
                    return new Tuple<bool, string>(result, "success");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tuple<bool, string> DeleteGroup(int groupId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var entity = context.Groups.SingleOrDefault(x => x.Id == groupId);
                    var memberGroup = context.MembersGroupsInfoes.Select(x => x.GroupId == groupId).ToList();
                    if (memberGroup != null)
                    {
                        return new Tuple<bool, string>(false, "這個類別有人選到了ㄟ，不能刪喔!");
                    }
                    if (entity != null)
                    {
                        context.Groups.Remove(entity);
                        var result = context.SaveChanges() == 1;
                        return new Tuple<bool, string>(true, "success");
                    }
                    return new Tuple<bool, string>(false, "沒有這個id的類別");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MembersGroupsInfoDto> GetGroupMembers(int groupId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var group = context.MembersGroupsInfoes.Where(x => x.GroupId == groupId).ToList();
                    var groupDto = _mapper.Map<List<MembersGroupsInfoDto>>(group);
                    return groupDto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}