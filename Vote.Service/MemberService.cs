﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Vote.Data;

namespace Vote.Service
{
    public class MemberService : BaseService
    {
        private static MemberService _instance;

        public static MemberService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MemberService();
                }
                return _instance;
            }
        }

        public List<MemberDto> GetAllMembers()
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.Members.Select(x => x).ToList();
                    //result.ForEach(x => x.Name = new List<Member>());
                    var dto = _mapper.Map<List<MemberDto>>(result);
                    return dto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MemberDto GetMemberDetailByToken(string token)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.Members.SingleOrDefault(x => x.Token.Equals(token));
                    var memberDto = _mapper.Map<MemberDto>(result);
                    return memberDto;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tuple<int, MemberDto> GetMemberDetail(int memberId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var result = context.Members.Include("Groups").SingleOrDefault(x => x.Id == memberId);
                    var memberDto = _mapper.Map<MemberDto>(result);
                    var topics = context.VoteTopics.Where(x => x.CreateMemberId == memberId).ToList();
                    int count = topics.Count();

                    return new Tuple<int, MemberDto>(count, memberDto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //會員修改自己的資料
        public Tuple<bool, string, MemberDto> MemberModifyDetail(int memberId, MemberDetail memberDetail)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var member = context.Members.SingleOrDefault(x => x.Id == memberId);
                    member.Name = memberDetail.name;
                    member.Gender = memberDetail.gender;
                    member.Birthday = memberDetail.birthday;
                    member.ModifyDate = DateTime.Now;
                    var result = context.SaveChanges() == 1;
                    var memberDto = _mapper.Map<MemberDto>(member);
                    return new Tuple<bool, string, MemberDto>(result, "success", memberDto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //修改密碼
        public Tuple<bool, string> ModifyPasssword(int memberId, MemberPassword memberPassword)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var member = context.Members.SingleOrDefault(x => x.Id == memberId);
                    //檢查舊密碼否正確
                    if (!member.Password.Equals(memberPassword.oldPassword.GetHashCode().ToString()))
                    {
                        return new Tuple<bool, string>(false, "舊密碼不符");
                    }
                    //檢查新舊密碼一不一樣
                    if (member.Password.Equals(memberPassword.newPassword.GetHashCode().ToString()))
                    {
                        return new Tuple<bool, string>(false, "新密碼與舊密碼相同");
                    }
                    //檢查密碼一不一樣
                    if (!memberPassword.newPassword.Equals(memberPassword.newPassword2))
                    {
                        return new Tuple<bool, string>(false, "輸入密碼不相同");
                    }
                    //檢查密碼格式
                    if (memberPassword.newPassword.Length < 6)
                    {
                        return new Tuple<bool, string>(false, "密碼至少要6位數");
                    }
                    //全是數字                                                              //全是字母
                    if (memberPassword.newPassword.All(char.IsDigit) || Regex.IsMatch(memberPassword.newPassword, @"^[\p{L}]+$"))
                    {
                        return new Tuple<bool, string>(false, "密碼須包含數字及英文");
                    }
                    member.Password = memberPassword.newPassword.GetHashCode().ToString();
                    member.ModifyDate = DateTime.Now;
                    var result = context.SaveChanges() == 1;
                    return new Tuple<bool, string>(result, "success");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //會員新增修改刪除類別
        public Tuple<bool, string, MemberDto> ModifyGroups(List<int> list, int memberId)
        {
            using (var context = new VoteEntities())
            {
                var listGroupsId = context.Groups.Select(x => x.Id).ToList();
                foreach (var item in list)
                {
                    if (!listGroupsId.Contains(item))
                    {
                        return new Tuple<bool, string, MemberDto>(false, "有沒有類別的Id喔", null);
                    }
                }
                var sql = @"Delete from MembersGroups where MemberId = {0}";
                try
                {
                    context.Database.ExecuteSqlCommand(sql, memberId);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                var sql2 = @"Insert into MembersGroups values ({0}, {1})";
                foreach (var i in list)
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand(sql2, memberId, i);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                var m = GetMemberDetail(memberId);
                return new Tuple<bool, string, MemberDto>(true, "success", m.Item2);
            }
        }

        //會員新增選項
        public Tuple<bool, string> AddTopicItem(VoteItemDto voteItem, int memberId)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var checkItemUnique = context.Database.SqlQuery<string>("Select Name from VoteItems where VoteTopicId = {0};", voteItem.VoteTopicId).ToList();
                    var topic = context.VoteTopics.Include("VoteItems").SingleOrDefault(x => x.Id == voteItem.VoteTopicId);
                    var data = _mapper.Map<VoteItem>(voteItem);

                    if (!topic.CanAddItems)
                    {
                        return new Tuple<bool, string>(false, "這不能新增選項喔");
                    }
                    foreach (var item in checkItemUnique)
                    {
                        if (data.Name.Equals(item))
                        {
                            return new Tuple<bool, string>(false, "選項重複了");
                        }
                    }
                    data.CreateMemberId = memberId;
                    data.CreatedDate = DateTime.Now;
                    context.VoteItems.Add(data);
                    var result = context.SaveChanges() == 1;

                    return new Tuple<bool, string>(result, "成功");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class MemberDetail
    {
        public string name;
        public string gender;
        public DateTime birthday;
    }

    public class MemberPassword
    {
        public string oldPassword;
        public string newPassword;
        public string newPassword2;
    }
}