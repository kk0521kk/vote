﻿using AutoMapper;
using Vote.Data;

namespace Vote.Service
{
    public class BaseService
    {
        protected IMapper _mapper;

        protected BaseService()
        {
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<VoteTopic, VoteTopicDto>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<VoteTopicDto, VoteTopic>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<VoteItem, VoteItemDto>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<VoteItemDto, VoteItem>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<VoteItemsMembersInfoDto, VoteItemsMembersInfo>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<VoteItemsMembersInfo, VoteItemsMembersInfoDto>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();

            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<Group, GroupDto>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<GroupDto, Group>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<Member, MemberDto>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<MemberDto, Member>().IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();
            _mapper = new MapperConfiguration(cfg => cfg.CreateMap<VoteItemsMembersInfo, MemberDto>()
            .ForMember(d => d.Id, opt => opt.MapFrom(s => s.MemberId))
            .ForMember(d => d.Name, opt => opt.MapFrom(s => s.MemberName))
            .IgnoreAllPropertiesWithAnInaccessibleSetter()).CreateMapper();

            //Mapper.Initialize(cfg =>
            //{
            //cfg.CreateMap<VoteTopic, VoteTopicDto>();
            //cfg.CreateMap<VoteTopicDto, VoteTopic>();
            //cfg.CreateMap<VoteItem, VoteItemDto>();
            //cfg.CreateMap<VoteItemDto, VoteItem>();
            //});
        }
    }
}