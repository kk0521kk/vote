﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Vote.Data;

namespace Vote.Service
{
    public class IdentityService : BaseService
    {
        private static IdentityService _instance;

        public static IdentityService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new IdentityService();
                }
                return _instance;
            }
        }

        public Tuple<bool, string, Member> Register(MemberDto memberDto)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var member = _mapper.Map<Member>(memberDto);
                    if (!Regex.IsMatch(memberDto.Email, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
                    {
                        return new Tuple<bool, string, Member>(false, "email格式錯誤", null);
                    }
                    var checkEmailUnique = context.Database.SqlQuery<string>("Select Email from Members;").ToList();
                    //檢查重複email
                    foreach (var item in checkEmailUnique)
                    {
                        if (item.Equals(memberDto.Email))
                        {
                            return new Tuple<bool, string, Member>(false, "email已被註冊", null);
                        }
                    }
                    //檢查密碼格式
                    if (memberDto.Password.Length < 6)
                    {
                        return new Tuple<bool, string, Member>(false, "密碼至少要6位數", null);
                    }
                    //全是數字                      //全是字母
                    if (memberDto.Password.All(char.IsDigit) || Regex.IsMatch(memberDto.Password, @"^[\p{L}]+$"))
                    {
                        return new Tuple<bool, string, Member>(false, "密碼須包含數字及英文", null);
                    }
                    //檢查2個密碼一不一樣
                    if (!memberDto.Password.Equals(memberDto.Password2))
                    {
                        return new Tuple<bool, string, Member>(false, "2個密碼不一致", null);
                    }
                    if (!memberDto.Gender.Equals("M") && !memberDto.Gender.Equals("F"))
                    {
                        return new Tuple<bool, string, Member>(false, "性別幫我用成M or F", null);
                    }

                    member.CreateDate = DateTime.Now;
                    member.ModifyDate = DateTime.Now;

                    member.Password = memberDto.Password.GetHashCode().ToString();
                    member.Password2 = member.Password;
                    context.Members.Add(member);

                    var result = context.SaveChanges() == 1;
                    return new Tuple<bool, string, Member>(result, "success", member);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tuple<bool, string, MemberDto> Login(Mem mem)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    var pwd = mem.Password.GetHashCode().ToString();
                    var member = context.Members.Include("Groups").
                        SingleOrDefault(x => x.Email.Equals(mem.Email) && x.Password.Equals(pwd));
                    if (member == null)
                    {
                        return new Tuple<bool, string, MemberDto>(false, "帳號或密碼錯誤", null);
                    }
                    if (member.Token == null)
                    {
                        member.Token = Guid.NewGuid().ToString();
                        context.SaveChanges();
                    }
                    var memberDto = _mapper.Map<MemberDto>(member);
                    if (memberDto.IsAdmin)
                    {
                        return new Tuple<bool, string, MemberDto>(true, "登入管理員", memberDto);
                    }
                    return new Tuple<bool, string, MemberDto>(true, "登入一般會員", memberDto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Logout(string token)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    Debug.WriteLine("Token: " + token);
                    var member = context.Members.SingleOrDefault(x => x.Token.Equals(token));
                    if (member != null)
                    {
                        member.Token = null;
                        context.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class Mem
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}