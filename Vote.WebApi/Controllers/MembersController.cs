﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Vote.Data;
using Vote.Service;
using Vote.WebApi;

namespace Demo.WebApi.Controllers
{
    [TokenAuthorize]
    [RoutePrefix("api/Members")]
    public class MembersController : BaseController
    {
        // Get/Members/GetAllMembers
        [HttpGet]
        [Route("GetAllMembers")]
        public IHttpActionResult GetAllMembers()
        {
            try
            {
                if (!Member.IsAdmin)
                {
                    logger.Info("GetAllMembers(401), Token: " + Member.Token + "IsAdmin: " + Member.IsAdmin);
                    return Unauthorized();
                }
                var result = MemberService.Instance.GetAllMembers();
                if (result == null)
                    return NotFound();
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetAllMembers: " + ex.Message, ex);
                throw;
            }
        }

        // Get/Members/detail
        [HttpGet]
        [Route("detail")]
        public IHttpActionResult GetMemberDetail()
        {
            try
            {
                var result = MemberService.Instance.GetMemberDetail(Member.Id);
                var group = GroupService.Instance.GetAllGroupsWithMemberGroups(Member.Id);
                var topicCount = VoteService.Instance.GetCountMemberVotedTopics(Member.Id);
                if (result == null)
                    return NotFound();

                return Ok(new { CountVotedTopics = topicCount, CountGroups = group.Item1, CountAskedTimes = result.Item1, Member = result.Item2 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetMemberDetail: " + ex.Message, ex);
                throw ex;
            }
        }

        // Post/Members/RegisterOrModify
        [HttpPost]
        [Route("MemberModifyDetail")]
        public IHttpActionResult MemberModifyDetail(MemberDetail memberDetail)
        {
            try
            {
                var result = MemberService.Instance.MemberModifyDetail(Member.Id, memberDetail);
                return Ok(new { Success = result.Item1, Message = result.Item2, result = result.Item3 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("MemberModifyDetail: " + ex.Message, ex);
                throw ex;
            }
        }

        //POST api/Members
        [HttpPost]
        [Route("ModifyGroups")]
        public IHttpActionResult ModifyGroups(List<int> list)
        {
            try
            {
                var result = MemberService.Instance.ModifyGroups(list, Member.Id);
                if (result.Item3 != null)
                {
                    var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
                    var existingClaim = identity.FindFirst(Member.Token);
                    if (existingClaim != null)
                        identity.RemoveClaim(existingClaim);
                    // add new claim
                    identity.AddClaim(new Claim(result.Item3.Token, JsonConvert.SerializeObject(HttpRuntime.Cache[result.Item3.Token] as MemberDto)));
                }

                return Ok(new { Success = result.Item1, Messsage = result.Item2, Result = result.Item3 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("ModifyGroups: " + ex.Message, ex);
                throw ex;
            }
        }

        //POST api/Members
        [HttpPost]
        [Route("AddVoteItem")]
        public IHttpActionResult AddVoteItem(VoteItemDto voteItem)
        {
            try
            {
                var result = MemberService.Instance.AddTopicItem(voteItem, Member.Id);
                var topic = VoteService.Instance.GetVoteTopicAndItems(voteItem.VoteTopicId);
                return Ok(new { Success = result.Item1, Message = result.Item2, result = topic });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("AddVoteItem: " + ex.Message, ex);
                throw ex;
            }
        }

        //POST api/Members
        [HttpPost]
        [Route("ModifyPassword")]
        public IHttpActionResult ModifyPassword([FromBody]MemberPassword memberPassword)
        {
            try
            {
                var result = MemberService.Instance.ModifyPasssword(Member.Id, memberPassword);
                return Ok(new { Success = result.Item1, Message = result.Item2 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("ModifyPassword" + ex.Message, ex);
                throw ex;
            }
        }
    }
}