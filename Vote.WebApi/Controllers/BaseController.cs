﻿using log4net;
using Newtonsoft.Json;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using Vote.Data;

namespace Demo.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        //log4net
        public static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Token => Request.Headers.Contains("Token") ? Request.Headers.GetValues("Token").FirstOrDefault() : "";

        public MemberDto Member
        {
            get
            {
                var identity = User.Identity as ClaimsIdentity;
                if (User == null || identity == null) return null;
                var member = identity.FindFirst(Token) == null ? null : JsonConvert.DeserializeObject<MemberDto>(identity.FindFirst(Token).Value);
                return member;
            }
        }
    }
}