﻿using Demo.WebApi.Controllers;
using System.Linq;
using System.Web.Http;
using Vote.Data;
using Vote.Service;

namespace Vote.WebApi.Controllers
{
    [TokenAuthorize]
    [RoutePrefix("api/Groups")]
    public class ValuesController : BaseController
    {
        // GET api/Groups/GetAllGroups
        [HttpGet]
        [Route("GetAllGroups")]
        public IHttpActionResult GetAllGroups()
        {
            try
            {
                if (!Member.IsAdmin)
                {
                    logger.Info("GetAllGroups(401), Token: " + Member.Token + "IsAdmin: " + Member.IsAdmin);
                    return Unauthorized();
                }
                var result = GroupService.Instance.GetAllGroups();
                logger.Info("GetAllGroups: " + result);
                if (result.Count() == 0)
                    return NotFound();
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetAllGroups: " + ex.Message, ex);
                throw ex;
            }
        }

        // GET api/Groups/GetGroupTopics/1
        [HttpGet]
        [Route("GetGroupVoteTopics/{groupId}")]
        public IHttpActionResult GetGroupTopics(int groupId)
        {
            try
            {
                var result = GroupService.Instance.GetGroupVoteTopics(groupId);
                if (result == null)
                    return NotFound();
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetGroupTopics: " + ex.Message, ex);
                throw ex;
            }
        }

        //GET api/Groups/GetAllGroupsWithMemberGroups
        [HttpGet]
        [Route("GetAllGroupsWithMemberGroups")]
        public IHttpActionResult GetAllGroupsWithMemberGroups()
        {
            try
            {
                var result = GroupService.Instance.GetAllGroupsWithMemberGroups(Member.Id);
                if (result.Item2.Count == 0)
                {
                    return NotFound();
                }
                return Ok(new { CountGroup = result.Item1, Groups = result.Item2 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetAllGroupsWithMemberGroups: " + ex.Message, ex);
                throw ex;
            }
        }

        // GET api/Groups/GetGroupMembers
        [HttpGet]
        [Route("GetGroupMembers/{groupId}")]
        public IHttpActionResult GetGroupMembers(int groupId)
        {
            try
            {
                if (!Member.IsAdmin)
                {
                    return Unauthorized();
                }
                var result = GroupService.Instance.GetGroupMembers(groupId);
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetGroupMembers: " + ex.Message, ex);
                throw ex;
            }
        }

        // GET api/Groups/AddGroups            管理員新增類別
        [HttpPost]
        [Route("AddGroups")]
        public IHttpActionResult AddGroups(GroupDto group)
        {
            try
            {
                if (!Member.IsAdmin)
                {
                    return Unauthorized();
                }
                var result = GroupService.Instance.AddGroups(group);
                var groups = GroupService.Instance.GetAllGroups();
                return Ok(new { Success = result.Item1, Message = result.Item2, Result = groups });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("AddGroups" + ex.Message, ex);
                throw ex;
            }
        }

        // GET api/Groups/DeleteGroups          管理員刪除類別
        [HttpGet]
        [Route("DeleteGroup/{id}")]
        public IHttpActionResult DeleteGroup(int id)
        {
            try
            {
                if (!Member.IsAdmin)
                {
                    return Unauthorized();
                }
                var result = GroupService.Instance.DeleteGroup(id);
                return Ok(new { Success = result.Item1, Message = result.Item2 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("DeleteGroup: " + ex.Message, ex);
                throw ex;
            }
        }
    }
}