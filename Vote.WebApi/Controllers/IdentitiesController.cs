﻿using Newtonsoft.Json;
using System;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Vote.Data;
using Vote.Service;

namespace Demo.WebApi.Controllers
{
    [RoutePrefix("api/Identities")]
    public class IdentitiesController : BaseController
    {
        //api/Identities/Register
        [HttpPost]
        [Route("Register")]
        [AllowAnonymous]
        public IHttpActionResult Register(MemberDto memberDto)
        {
            try
            {
                var result = IdentityService.Instance.Register(memberDto);
                return Ok(new { Success = result.Item1, Message = result.Item2, Result = result.Item3 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("Register: " + ex.Message, ex);
                throw ex;
            }
        }

        //api/Identities/Login
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public IHttpActionResult Login(Mem mem)
        {
            if (mem == null)
            {
                return BadRequest();
            }
            try
            {
                var result = IdentityService.Instance.Login(mem);
                if (result.Item1)
                {
                    HttpRuntime.Cache.Insert(result.Item3.Token, result.Item3, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 20));
                    var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
                    var existingClaim = identity.FindFirst(result.Item3.Token);
                    if (existingClaim != null)
                        identity.RemoveClaim(existingClaim);
                    // add new claim
                    identity.AddClaim(new Claim(result.Item3.Token, JsonConvert.SerializeObject(HttpRuntime.Cache[result.Item3.Token] as MemberDto)));

                    logger.Info("Login Success" + result.Item3.Name);
                }

                return Ok(new { Success = result.Item1, Message = result.Item2, Result = result.Item3 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("Login" + ex.Message, ex);
                throw ex;
            }
        }

        //api/Identities/Logout
        [HttpGet]
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            try
            {
                var result = IdentityService.Instance.Logout(Token);
                if (result)
                {
                    HttpRuntime.Cache.Remove(Token);
                    return Ok(result);
                }
                return NotFound();
            }
            catch (System.Exception ex)
            {
                logger.Fatal("Logout: " + ex.Message, ex);
                throw ex;
            }
        }
    }
}