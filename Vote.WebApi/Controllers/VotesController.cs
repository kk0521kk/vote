﻿using Demo.WebApi.Controllers;
using System.Linq;
using System.Web.Http;
using Vote.Data;
using Vote.Service;

namespace Vote.WebApi.Controllers
{
    [TokenAuthorize]
    [RoutePrefix("api/Votes")]
    public class VotesController : BaseController
    {
        // GET api/Votes
        [HttpGet]
        [Route("GetAllVoteTopics")]
        public IHttpActionResult GetAllVoteTopics()
        {
            try
            {
                var result = VoteService.Instance.GetAllVoteTopics();
                if (result.Count() == 0)
                    return NotFound();
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetAllVoteTopics" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetVoteTopicAndItems/{id}")]
        public IHttpActionResult GetVoteTopicAndItems(int id)
        {
            try
            {
                var result = VoteService.Instance.GetVoteTopicAndItems(id);
                if (result == null)
                    return NotFound();
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetVoteTopicAndItems" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetTopicsByKey/{key}")]
        public IHttpActionResult GetTopicsByKey(string key)
        {
            try
            {
                var result = VoteService.Instance.GetTopicsByKey(key);
                if (result.Count() == 0)
                    return NotFound();
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetTopicsByKey" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpPost]
        [Route("AddVoteTopic")]
        public IHttpActionResult AddVoteTopic(VoteTopicDto data)
        {
            try
            {
                var result = VoteService.Instance.SaveVoteTopic(data, Member.Id);
                return Ok(new { Success = result.Item1, Messsage = result.Item2, Result = result.Item3 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("AddVoteTopic" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("DeleteVoteTopic/{id}")]
        public IHttpActionResult DeleteVoteTopic(int id)
        {
            try
            {
                var result = VoteService.Instance.DeleteVoteTopic(Member.Id, id);
                if (result == 1)
                    return Ok();
                return NotFound();
            }
            catch (System.Exception ex)
            {
                logger.Fatal("DeleteVoteTopic" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpPost]
        [Route("MemberVote")]
        public IHttpActionResult MemberVote(VoteTopicDto data)
        {
            try
            {
                int memberId = Member.Id;
                var result = VoteService.Instance.MemberVote(data, memberId);
                var topic = VoteService.Instance.GetVoteResult(data.Id, memberId);
                if (topic != null)
                {
                    return Ok(new { Success = result.Item1, Message = result.Item2, Result = topic });
                }
                return Ok(new { Success = result.Item1, Message = result.Item2 });
            }
            catch (System.Exception ex)
            {
                logger.Fatal("MemberVote: " + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("MemberAskedVoteTopics")]
        public IHttpActionResult MemberAskedVoteTopics()
        {
            try
            {
                var result = VoteService.Instance.GetMemberAskedVoteTopics(Member.Id);
                if (result.Count == 0)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("MemberAskedVoteTopics: " + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("MemberGroupTopics")]
        public IHttpActionResult MemberGroupTopics()
        {
            try
            {
                var result = VoteService.Instance.GetMemberVoteTopics();
                if (result.Count == 0)
                    return NotFound();

                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("MemberGroupTopics" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetVoteResult/{TopicId}")]
        public IHttpActionResult GetVoteResult(int TopicId)
        {
            try
            {
                var result = VoteService.Instance.GetVoteResult(TopicId, Member.Id);
                if (result == null)
                    return NotFound();

                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetVoteResult" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetVoteResultDetail/{TopicId}")]
        public IHttpActionResult GetVoteResultDetail(int TopicId)
        {
            try
            {
                var result = VoteService.Instance.GetVoteResultDetail(TopicId);
                if (result == null)
                    return NotFound();

                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetVoteResultDetail" + ex.Message, ex);
                throw ex;
            }
        }

        [HttpGet]
        [Route("GetDoneMemberVoteTopics")]
        public IHttpActionResult GetDoneMemberVoteTopics()
        {
            try
            {
                var result = VoteService.Instance.GetDoneMemberVoteTopics();
                if (result.Count == 0)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (System.Exception ex)
            {
                logger.Fatal("GetDoneMemberVoteTopics" + ex.Message, ex);
                throw ex;
            }
        }
    }
}