﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Vote.Data;
using Vote.Service;

namespace Vote.WebApi
{
    public class TokenAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            return GetIsAuthorized(actionContext);
        }

        private bool GetIsAuthorized(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Headers.Contains("Token"))
                return false;
            var token = actionContext.Request.Headers.GetValues("Token").FirstOrDefault();
            if (HttpRuntime.Cache[token] == null)
            {
                var member = CheckToken(token);
                if (member == null) return false;
                HttpRuntime.Cache.Insert(token, member, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 20));
            }
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            var existingClaim = identity.FindFirst(token);
            if (existingClaim != null)
                identity.RemoveClaim(existingClaim);
            // add new claim
            identity.AddClaim(new Claim(token, JsonConvert.SerializeObject(HttpRuntime.Cache[token] as MemberDto)));

            return true;
        }

        public MemberDto CheckToken(string token)
        {
            try
            {
                using (var context = new VoteEntities())
                {
                    return MemberService.Instance.GetMemberDetailByToken(token);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}